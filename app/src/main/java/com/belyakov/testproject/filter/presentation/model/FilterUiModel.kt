package com.belyakov.testproject.filter.presentation.model

data class FilterUiModel(
    val value: String?,
    val title: String
)