package com.belyakov.testproject.base.presentation.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val Gray300 = Color(0xFFe0e0e0)
val Gray500 = Color(0xFF9e9e9e)
val Cyan100 = Color(0xFFb2ebf2)
val Cyan300 = Color(0xFF4DD0E1)
val BlueGray100 = Color(0xFFCFD8DC)
val BlueGray300 = Color(0xFFb0bec5)