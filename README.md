# Introduction

In order to build the project you need an API-key. 
It's possible to build project via gitlab (the key is stored in private CI/CD variables)

Or if you want to build the project locally, 
you can add the following line in the local.properties file:
API_KEY = "b536d80abd80435284fda380910aebb7"
